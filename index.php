<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Admin Pages</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

  <!-- bootstrap -->
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - header sidebar di atas -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
        <div class="sidebar-brand-text mx-3">SPK TOPSIS LAPTOP</div>
      </a>
      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <?php
            if(@$_GET['a']=='data_laptop'){
              $active1='class="nav-item active"';
              $active2='';
              $active3='';
              $active4='';
              $active5='';
            }else if(@$_GET['a']=='kriteria'){
              $active1='';
              $active2='class="nav-item active"';
              $active3='';
              $active4='';
              $active5='';
            }else if(@$_GET['a']=='nilai_matrix'){
              $active1='';
              $active2='';
              $active3='class="nav-item active"';
              $active4='';
              $active5='';
            }else if(@$_GET['a']=='kepentingan'){
              $active1='';
              $active2='';
              $active3='';
              $active4='class="nav-item active"';
              $active5='';
            }else if(@$_GET['a']=='hasil_topsis'){
              $active1='';
              $active2='';
              $active3='';
              $active4='';
              $active5='class="nav-item active"';
            }else{
              $active1='';
              $active2='';
              $active3='';
              $active4='';
              $active5='';
            }	
          ?> 

      <!-- Manu - Dashboard -->
      <li <?php echo $active1 ?> class="nav-item">
        <a class="nav-link" href="?a=data_laptop&k=data_laptop">
          <i class="fas fa-fw fa-folder"></i>
          <span>Data Laptop</span></a>
      </li>

      <hr class="sidebar-divider my-0">

      <li <?php echo $active2 ?> class="nav-item" >
        <a class="nav-link" href="?a=kriteria&k=kriteria">
          <i class="fas fa-fw fa-folder"></i>
          <span>Kriteria</span></a>
      </li>

      <hr class="sidebar-divider my-0">

      <li <?php echo $active3 ?> class="nav-item" >
        <a class="nav-link" href="?a=nilai_matrix&k=nilai_matrix">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Nilai Matrix</span></a>
      </li>

      <hr class="sidebar-divider my-0">

      <li <?php echo $active4 ?> class="nav-item" >
        <a class="nav-link" href="?a=kepentingan&k=kepentingan">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Kepentingan</span></a>
      </li>

      <hr class="sidebar-divider my-0">

      <li <?php echo $active5 ?> class="nav-item" >
        <a class="nav-link" href="?a=hasil_topsis&k=hasil_topsis">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Hasil Topsis</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

           <!-- Nav Item - menu logout untuk admin-->
           <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 ">Valerie Luna</span>
                <div class="sidebar-brand-icon rotate-n-15">
                  <i class="fas fa-user"></i>
                </div>
                <!-- <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60"> -->
              </a>
              <!-- Dropdown - User logout -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>
          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Content Row -->
          <?php
              if(@$_GET['a']=='data_laptop'){
                include ("./contents/data_laptop/index.php");
              }else if(@$_GET['a']=='kriteria'){
                include ("./contents/kriteria/index.php");
              }else if(@$_GET['a']=='nilai_matrix'){
                include ("./contents/matriks/matriks.php");
              }else if(@$_GET['a']=='kepentingan'){
                include ("./contents/kepentingan/kepentingan.php");
              }else if(@$_GET['a']=='hasil_topsis'){
                include ("./contents/hasil_topsis/index.php");
              }else {
                include ("./contents/welcome.php");
              }
          ?>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
<br>
      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button -->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal -->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.php">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>

  <!-- bootstrap js
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> -->

</body>

</html>
