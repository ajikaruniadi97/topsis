<div class="card shadow mb-4">
    <div class="card-body">
        <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <form class="col-md-12">
                  <div class="form-row">
                      <!-- input nama kriteria -->
                      <div class="form-group col-md-6">
                      <label for="namakriteria">NAMA KRITERIA</label>
                      <input type="text" class="form-control" id="namakriteria" placeholder="Input Nama Kriteria">
                      </div>
                      <!-- input bobot -->
                      <div class="form-group col-md-6">
                      <label for="bobot">BOBOT</label>
                      <input type="text" class="form-control" id="bobot" placeholder="Input Bobot">
                      </div>
                  </div>

                  <div class="form-row">
                      <!-- input point 1 -->
                      <div class="form-group col-md-6">
                      <label for="point1">POINT 1</label>
                      <input type="text" class="form-control" id="point1" placeholder="Input Point">
                      </div>
                      <!-- input point 2 -->
                      <div class="form-group col-md-6">
                      <label for="point2">POINT 2</label>
                      <input type="text" class="form-control" id="point2" placeholder="Input Point">
                      </div>
                  </div>

                  <div class="form-row">
                      <!-- input nama kriteria -->
                      <div class="form-group col-md-6">
                      <label for="point3">POINT 3</label>
                      <input type="text" class="form-control" id="point3" placeholder="Input Point">
                      </div>
                      <!-- input bobot -->
                      <div class="form-group col-md-6">
                      <label for="point4">POINT 4</label>
                      <input type="text" class="form-control" id="point5" placeholder="Input Point">
                      </div>
                  </div>

                  <div class="form-row">
                      <!-- input seri komputer -->
                      <div class="form-group col-md-6">
                      <label for="point5">POINT 5</label>
                      <input type="text" class="form-control" id="point5" placeholder="Input Point">
                      </div>
                      <!-- pilih disk laptop -->
                      <div class="form-group col-md-6">
                      <label for="inputDisk">SIFAT KRITERIA</label>
                      <select id="inputDisk" class="form-control">
                          <option selected>Choose...</option>
                          <option>Benefit</option>
                          <option>Cost</option>
                      </select>
                      </div>
                  </div>

                  <button type="submit" class="btn btn-primary">Input</button>
                  <span> </span>
                  <button type="reset" class="btn btn-primary">Cancel</button>
              </form>
            </table>
        </div>
    </div>
</div>