<!-- Page Heading -->
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
    <div class="card-body">
        <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>NO</th>
                <th>Nama Kriteria</th>
                <th>Point 1</th>
                <th>Point 2</th>
                <th>Point 3</th>
                <th>Point 4</th>
                <th>Point 5</th>
                <th>Sifat Kriteria</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>NO</th>
                <th>Nama Kriteria</th>
                <th>Point 1</th>
                <th>Point 2</th>
                <th>Point 3</th>
                <th>Point 4</th>
                <th>Point 5</th>
                <th>Sifat Kriteria</th>
                <td>Actions</td>
            </tr>
            </tfoot>
            <tbody>
            <tr>
                <td>Tiger Nixon</td>
                <td>System Architect</td>
                <td>Edinburgh</td>
                <td>6111111111</td>
                <td>2011/04/25</td>
                <td>$320,800</td>
                <td>$320,800</td>
                <td>$320,800</td>
                <td>
                    <button type="button" class="btn btn-success btn-sm">Ubah </button>
                    <button type="button" class="btn btn-danger btn-sm">Hapus</button>
                </td>
            </tr>
            </tbody>
        </table>
        </div>
    </div>
</div>