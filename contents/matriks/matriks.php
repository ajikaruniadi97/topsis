<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Nilai Matriks</h1>
</div>

<table class="table">
    <tr>
        <td width="150">Id Alternatif</td>
        <td>
            <div class="form-group col-md-11">
            <select class="form-control">
                <option selected>Choose...</option>
                <option>...</option>
                </select>
            </div>
        </td>
    </tr>
</table>
<hr class="sidebar-divider my-0">
<table class="table">
    <!-- matriks harga -->
    <tr>
        <td width="180">Harga</td>
        <td>
            <input type="radio">
            <span>1</span>
        </td>
        <td>
            <input type="radio">
            <span>2</span>
        </td>
        <td>
            <input type="radio">
            <span>3</span>
        </td>
        <td>
            <input type="radio">
            <span>4</span>
        </td>
        <td>
            <input type="radio">
            <span>5</span>
        </td>
    </tr>
    <!-- matriks ram -->
    <tr>
        <td width="180">Ram</td>
        <td>
            <input type="radio">
            <span>1</span>
        </td>
        <td>
            <input type="radio">
            <span>2</span>
        </td>
        <td>
            <input type="radio">
            <span>3</span>
        </td>
        <td>
            <input type="radio">
            <span>4</span>
        </td>
        <td>
            <input type="radio">
            <span>5</span>
        </td>
    </tr>
    <!-- Matriks Merk processor -->
    <tr>
        <td width="180">Merk Processor</td>
        <td>
            <input type="radio">
            <span>1</span>
        </td>
        <td>
            <input type="radio">
            <span>2</span>
        </td>
        <td>
            <input type="radio">
            <span>3</span>
        </td>
        <td>
            <input type="radio">
            <span>4</span>
        </td>
        <td>
            <input type="radio">
            <span>5</span>
        </td>
    </tr>
    <!-- matriks hard disk -->
    <tr>
        <td width="180">Harddisk</td>
        <td>
            <input type="radio">
            <span>1</span>
        </td>
        <td>
            <input type="radio">
            <span>2</span>
        </td>
        <td>
            <input type="radio">
            <span>3</span>
        </td>
        <td>
            <input type="radio">
            <span>4</span>
        </td>
        <td>
            <input type="radio">
            <span>5</span>
        </td>
    </tr>
    <!-- matriks vga -->
    <tr>
        <td width="180">VGA</td>
        <td>
            <input type="radio">
            <span>1</span>
        </td>
        <td>
            <input type="radio">
            <span>2</span>
        </td>
        <td>
            <input type="radio">
            <span>3</span>
        </td>
        <td>
            <input type="radio">
            <span>4</span>
        </td>
        <td>
            <input type="radio">
            <span>5</span>
        </td>
    </tr>
    <!-- matriks merk laptop -->
    <tr>
        <td width="180">Merk Laptop</td>
        <td>
            <input type="radio" value="point1">
            <span>1</span>
        </td>
        <td>
            <input type="radio" value="point2">
            <span>2</span>
        </td>
        <td>
            <input type="radio" value="point3">
            <span>3</span>
        </td>
        <td>
            <input type="radio" value="point4">
            <span>4</span>
        </td>
        <td>
            <input type="radio" value="point5">
            <span>5</span>
        </td>
    </tr>
    <tr>
        <td>
            <button type="submit" class="btn btn-primary">Input</button>
            <button type="reset" class="btn btn-primary">Cancel</button>
        </td>
    </tr>
</table>