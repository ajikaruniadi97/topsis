<div class="card shadow mb-4">
    <div class="card-body">
        <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <form class="col-md-12">
                  <div class="form-row">
                      <!-- input merek laptop -->
                      <div class="form-group col-md-6">
                      <label for="inputMerekLaptop">MERK LAPTOP</label>
                      <input type="text" class="form-control" id="inputMerekLaptop" placeholder="Input Merk Laptop">
                      </div>
                      <!-- pilih jenis ram -->
                      <div class="form-group col-md-6">
                      <label for="inputRam">RAM</label>
                      <select id="inputRam" class="form-control">
                          <option selected>Kapasitas Ram...</option>
                          <option>...</option>
                      </select>
                      </div>
                  </div>

                  <div class="form-row">
                      <!-- input seri komputer -->
                      <div class="form-group col-md-6">
                      <label for="inputSeri">SERI LAPTOP</label>
                      <input type="text" class="form-control" id="inputSeri" placeholder="Input Seri Laptop">
                      </div>
                      <!-- pilih disk laptop -->
                      <div class="form-group col-md-6">
                      <label for="inputDisk">HARDDISK</label>
                      <select id="inputDisk" class="form-control">
                          <option selected>Kapasitas Harddisk...</option>
                          <option>...</option>
                      </select>
                      </div>
                  </div>

                  <div class="form-row">
                      <!-- pilih disk laptop -->
                      <!-- input seri komputer -->
                      <div class="form-group col-md-6">
                      <label for="inputSeriProcessor">VGA</label>
                      <input type="text" class="form-control" id="inputSeriProcessor" placeholder="Input Merek VGA">
                      </div>
                      <!-- input seri komputer -->
                      <div class="form-group col-md-6">
                      <label for="inputHarga">HARGA</label>
                      <input type="text" class="form-control" id="inputHarga" placeholder="Input Harga">
                      </div>
                  </div>

                  <div class="form-row">
                      <div class="form-group col-md-12">
                      <label for="inputImage">Image</label>
                      <input type="file" class="form-control" id="inputImage">
                      </div>
                  </div>

                  <button type="submit" class="btn btn-primary">Input</button>
                  <span> </span>
                  <button type="reset" class="btn btn-primary">Cancel</button>
              </form>
            </table>
        </div>
    </div>
</div>