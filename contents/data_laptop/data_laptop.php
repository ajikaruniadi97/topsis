<!-- Page Heading -->
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
    <div class="card-body">
        <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>Id Laptop</th>
                <th>Merek Laptop</th>
                <th>Merek Processor</th>
                <th>Ram</th>
                <th>Harddisk</th>
                <th>VGA</th>
                <th>Harga<sup>(Rp)</sup></th>
                <th>Gambar</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Id Laptop</th>
                <th>Merek Laptop</th>
                <th>Merek Processor</th>
                <th>Ram</th>
                <th>Harddisk</th>
                <th>VGA</th>
                <th>Harga <sup>(Rp)</sup></th>
                <th>Gambar</th>
                <th>Actions</th>
            </tr>
            </tfoot>
            <tbody>
            <tr>
                <td>Tiger Nixon</td>
                <td>System Architect</td>
                <td>Edinburgh</td>
                <td>61</td>
                <td>2011/04/25</td>
                <td>$320,800</td>
                <td>$320,800</td>
                <td>$320,800</td>
                <td>
                    <button type="button" class="btn btn-success btn-sm">Ubah </button>
                    <button type="button" class="btn btn-danger btn-sm">Hapus</button>
                </td>
            </tr>
            
            </tbody>
        </table>
        </div>
    </div>
</div>